const { execSync } = require('child_process');
const { camelCase, upperFirst } = require('lodash');

const powersOfTwo = [8, 16, 32, 64, 128];
const nextPowerOfTwo = n => powersOfTwo.find(e => e >= n);

const enrichModel = model => {
  const name_type = upperFirst(camelCase(model.name));
  const name_function = name_type.toLowerCase();
  const value_type = `u${nextPowerOfTwo(model.width)}`;

  const table = execSync(`pycrc --width=${model.width} --poly=${model.poly} --reflect-in=${model.refin} --xor-in=${model.init} --reflect-out=${model.refout} --xor-out=${model.xorout} --generate=table`)
    .toString()
    .replace(/\s+/g, '')
    .slice(1, -1)
    .replace(/,/g, ', ');

  return {
    ...model,
    name_type,
    name_function,
    value_type,
    table,
  };
};

module.exports = { enrichModel };
