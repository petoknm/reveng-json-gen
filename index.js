const fs = require("fs");
const cheerio = require('cheerio');
const minimist = require('minimist');
const { zipWith, every, includes } = require('lodash');
const { enrichModel } = require('./enricher');

const args = minimist(process.argv.slice(2));

const input = fs.readFileSync(0, 'utf-8');
const $ = cheerio.load(input);

const codeRegex = /width=(?<width>\d+)  poly=(?<poly>0x.+?)  init=(?<init>0x.+?)  refin=(?<refin>.+?)  refout=(?<refout>.+?)  xorout=(?<xorout>0x.+?)  check=(?<check>0x.+?)  residue=(?<residue>0x.+?)  name=\"(?<name>.+)\"/;
const hexRegex = /^[0-9a-f]+$/gmi;

const guessCodewordFormat = codeword =>
  every(codeword, c => /[01]+/.test(c)) ? '0b' : '0x';

const addCodewordFormat = codeword =>
  `${guessCodewordFormat(codeword)}${codeword}`;

const headings = $('h3').toArray()
  .filter(h => $(h).text().startsWith('CRC-'));

const codeElements = $(headings).next().find('code').toArray();
const ulElements = $(headings).next().next().toArray();

const algorithms = codeElements
  .map(code => $(code).text().match(codeRegex))
  .map(matches => matches && matches.groups)
  .map(({width, refin, refout, ...rest}) => ({ width: parseInt(width), refin: refin === 'true', refout: refout === 'true', ...rest }));

const codewords = ulElements
  .map(ul => $(ul).text().replace(/\u200b/g, '').match(hexRegex))
  .map(matches => matches && matches.map(addCodewordFormat) || [])

let models = zipWith(algorithms, codewords, (alg, codewords) => ({...alg, codewords}));

if (args.enrich) {
  models = models.map(enrichModel);
}

console.log(JSON.stringify(models, null, 2));
